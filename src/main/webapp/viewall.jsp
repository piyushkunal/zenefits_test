<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#bbb;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#594F4F;background-color:#E0FFEB;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;background-color:#9DE0AD;}
.tg .tg-joa2{font-weight:bold;font-size:16px;background-color:#9b9b9b;text-align:center;vertical-align:top}
.tg .tg-gba0{background-color:#efefef;font-size:13px;text-align:center;vertical-align:top}
</style>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Hello</title>
    <script>
    function doSearch() {
        var searchText = document.getElementById('searchTerm').value;
        var targetTable = document.getElementById('dataTable');
        var targetTableColCount;

        //Loop through table rows
        for (var rowIndex = 0; rowIndex < targetTable.rows.length; rowIndex++) {
            var rowData = '';

            //Get column count from header row
            if (rowIndex == 0) {
               targetTableColCount = targetTable.rows.item(rowIndex).cells.length;
               continue; //do not execute further code for header row.
            }

            //Process data rows. (rowIndex >= 1)
            for (var colIndex = 0; colIndex < targetTableColCount; colIndex++) {
                rowData += targetTable.rows.item(rowIndex).cells.item(colIndex).textContent;
            }

            //If search term is not found in row data
            //then hide the row, else show
            if (rowData.toLowerCase().indexOf(searchText.toLowerCase()) == -1)
                targetTable.rows.item(rowIndex).style.display = 'none';
            else
                targetTable.rows.item(rowIndex).style.display = 'table-row';
        }
    }
    </script>
</head>
<body>
<input type="text" id="searchTerm" class="search_box" onkeyup="doSearch()" />
<table id="dataTable" class="tg" style="undefined;table-layout: fixed; width: 1222px">
<colgroup>
<col style="width: 292px">
<col style="width: 300px">
<col style="width: 291px">
<col style="width: 339px">
</colgroup>

  <tr>
    <th class="tg-joa2">Name</th>
    <th class="tg-joa2">Location</th>
    <th class="tg-joa2">Department</th>
    <th class="tg-joa2">Salaries</th>
  </tr>
  <c:forEach items="${response}" var="employee">

  <tr>
    <td class="tg-gba0">${employee.name}</td>
    <td class="tg-gba0">${employee.location}</td>
    <td class="tg-gba0">${employee.department}</td>
    <td class="tg-gba0">${employee.salaries}</td>
  </tr>

  </c:forEach>

</table>
</body>
</html>