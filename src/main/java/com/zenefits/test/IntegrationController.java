package com.zenefits.test;

import com.zenefits.test.domain.Employee;
import com.zenefits.test.manager.IntegrationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.ws.Response;
import javax.xml.ws.ResponseWrapper;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
public class IntegrationController {

    @Autowired
    private IntegrationManager integrationManager;

    @RequestMapping("/viewall")
    public void viewAll(Model model) throws Exception {
        List<Employee> res = integrationManager.fetchAllEmployees();
        model.addAttribute("response", res);
    }
}
