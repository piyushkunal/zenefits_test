package com.zenefits.test.domain;

import java.util.List;

/**
 * Created by Piyush Kunal on 20/04/18.
 */
public class Employee {
    String name;
    String department;
    String location;
    List<String> salaries;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSalaries() {
        return salaries;
    }

    public void setSalaries(List<String> salaries) {
        this.salaries = salaries;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
