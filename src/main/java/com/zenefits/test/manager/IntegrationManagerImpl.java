package com.zenefits.test.manager;

import com.zenefits.test.domain.Employee;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piyush Kunal on 20/04/18.
 */
@Component
public class IntegrationManagerImpl implements IntegrationManager {

    @Value("${zenefits.base.url}")
    private String baseUrl;

    @Value("${zenefits.company.id}")
    private String companyId;

    private HttpEntity<String> getReqHeaders() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.set("Authorization", "Bearer VPUCvXsCTfysjQb/wKUg");
        HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
        return requestEntity;
    }

    private JSONObject fetchResource(String url) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, getReqHeaders(), String.class);
        String res =  response.getBody();
        return new JSONObject(res);
    }

    private String createUrl(String inclusions) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(baseUrl);
        buffer.append("/companies");
        buffer.append("/"+companyId);
        buffer.append("/people?includes="+inclusions);
        return buffer.toString();
    }

    @Override
    public List<Employee> fetchAllEmployees() throws UnsupportedEncodingException{
        List<Employee> finalList = new ArrayList<>();
        String nextUrl = createUrl("employments+department+location");
        while(!StringUtils.isEmpty(nextUrl)) {
            nextUrl = java.net.URLDecoder.decode(nextUrl, "UTF-8");
            System.out.println("Fetching resource for: "+nextUrl);
            JSONObject topDoc = fetchResource(nextUrl);
            JSONObject nestedDoc = topDoc.getJSONObject("data");

            // iterate through nested doc
            JSONArray employeesArray = nestedDoc.getJSONArray("data");
            for (int i = 0;i< employeesArray.length();i++) {
                try {
                    JSONObject employee = employeesArray.getJSONObject(i);
                    Employee employeeDisplay = new Employee();
                    employeeDisplay.setName(employee.getString("first_name") + " " + employee.getString("last_name"));

                    // Fetch location
                    JSONObject location = !employee.isNull("location")?employee.getJSONObject("location"):null;
                    String city = location!=null?location.getString("city"):null;
                    employeeDisplay.setLocation(city);

                    // Fetch salary
                    JSONObject employments = !employee.isNull("employments")?employee.getJSONObject("employments"):null;
                    JSONArray salaryArray = employments!=null?employments.getJSONArray("data"):new JSONArray();
                    List<String> salaries = new ArrayList<>();
                    for(int j = 0;j<salaryArray.length();j++) {
                        JSONObject salary = salaryArray.getJSONObject(j);
                        salaries.add(!salary.isNull("annual_salary")?salary.getString("annual_salary"):null);
                    }
                    employeeDisplay.setSalaries(salaries);


                    // Fetch department
                    JSONObject department  = !employee.isNull("department")?employee.getJSONObject("department"):null;
                    String departmentName = department!=null?department.getString("name"):null;
                    employeeDisplay.setDepartment(departmentName);
                    finalList.add(employeeDisplay);
                } catch (Exception e) {
                    continue;
                }
            }
            JSONObject topData = !topDoc.isNull("data")?topDoc.getJSONObject("data"):null;
            nextUrl = !topData.isNull("next_url")?topData.getString("next_url"):null;
        }
        return finalList;
    }
}
