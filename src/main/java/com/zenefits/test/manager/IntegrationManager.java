package com.zenefits.test.manager;

import com.zenefits.test.domain.Employee;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Piyush Kunal on 20/04/18.
 */
public interface IntegrationManager {

    List<Employee> fetchAllEmployees() throws UnsupportedEncodingException;

}
